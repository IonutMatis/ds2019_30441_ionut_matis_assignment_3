package view;

import grpc.GRPCMedicationToMedicationPlan;
import lombok.Getter;
import lombok.Setter;
import utils.MyUtils;

import javax.swing.*;
import java.awt.*;

@Getter
@Setter
public class MedicationPanel extends JPanel {
    private JButton takeButton;
    private GRPCMedicationToMedicationPlan medToMedPlan;
    private boolean medicationTaken;
    private boolean messageSent;

    MedicationPanel(GRPCMedicationToMedicationPlan medToMedPlan) {
        this.medToMedPlan = medToMedPlan;
        medicationTaken = false;
        messageSent = false;
        this.setLayout(new FlowLayout(FlowLayout.CENTER, 50, 10));

        JLabel medicationNameLabel = new JLabel(medToMedPlan.getMedicationName());
        medicationNameLabel.setFont(MyUtils.TNR_PLAIN30);
        this.add(medicationNameLabel);

        JLabel intakeIntervalLabel = new JLabel(medToMedPlan.getIntakeInterval());
        intakeIntervalLabel.setFont(MyUtils.TNR_PLAIN30);
        this.add(intakeIntervalLabel);

        takeButton = new JButton("Take");
        takeButton.setBackground(Color.green);
        takeButton.setFont(MyUtils.TNR_PLAIN30);

        this.add(takeButton);
    }
}
