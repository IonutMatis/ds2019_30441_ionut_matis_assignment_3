import React from 'react';
import MyNavbar from "./navbar/MyNavbar";
import "../../style/HomePage.css";
import NotLoggedIn from "./NotLoggedIn";
import ResourceNotFound from "./ResourceNotFound";

const DoctorHomePage = ({userModelState}) => (
    <div>
        <MyNavbar userModelState={userModelState}/>
        {
            userModelState.currentUser.role === "anonymous"
                ?
                <NotLoggedIn/>
                :
                userModelState.currentUser.role !== "doctor"
                    ?
                    <ResourceNotFound/>
                    :

                    <div className="container">
                        <div className="jumbotron">
                            <h1 className="display-4">Hello, doctor!</h1>
                            <p className="lead">
                                This is the main page of your role. You can use the top menu to perform various actions.
                            </p>
                        </div>
                    </div>
        }
    </div>
);

export default DoctorHomePage;