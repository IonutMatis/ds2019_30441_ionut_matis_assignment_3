import React, {Component} from 'react';
import medicationModel from "../../../model/medicationModel";
import MedicationAddEdit from "../../dumb/medication/MedicationAddEdit";
import medicationPresenter from "../../../presenter/medicationPresenter";
import userModel from "../../../model/userModel";

const mapModelStateToComponentState = (medicationModelState) => ({
    editedMedication: medicationModelState.editedMedication
});

export default class SmartMedicationEdit extends Component {
    constructor(props) {
        super(props);

        let editedMedicationId = parseInt(props.match.params.id);
        medicationModel.setEditedMedication(editedMedicationId);
        this.state = mapModelStateToComponentState(medicationModel.state);
        this.listener = () =>
            this.setState(mapModelStateToComponentState(medicationModel.state));
        medicationModel.addListener("changeMedication", this.listener);
    }

    componentWillUnmount() {
        medicationModel.removeListener("changeMedication", this.listener);
    }

    render() {
        return (
            <MedicationAddEdit onChange={medicationPresenter.onEditChange}
                               onSubmit={medicationPresenter.onEdit}
                               medication={this.state.editedMedication}
                               mainText="Edit a medication"
                               userModelState={userModel.state}/>
        )
    }
}