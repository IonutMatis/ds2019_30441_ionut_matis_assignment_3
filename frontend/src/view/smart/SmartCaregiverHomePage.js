import React, {Component} from "react";
import userModel from "../../model/userModel";
import CaregiverHomePage from "../dumb/CaregiverHomePage";

const mapModelStateToComponentState = (userModel) => ({
    userModelState: userModel.state
});

export default class SmartCaregiverHomePage extends Component {
    constructor(props) {
        super(props);
        this.state = mapModelStateToComponentState(userModel);
        this.listener = (userModel) =>
            this.setState(mapModelStateToComponentState(userModel));
        userModel.addListener("userChange", this.listener);
    }

    componentWillUnmount() {
        userModel.removeListener("userChange", this.listener);
    }

    render() {
        return (
            <CaregiverHomePage userModelState={this.state.userModelState}/>
        );
    }
}