import React, {Component} from 'react';
import patientModel from "../../../model/patientModel";
import PatientAddEdit from "../../dumb/patient/PatientAddEdit";
import patientPresenter from "../../../presenter/patientPresenter";
import userModel from "../../../model/userModel";
import caregiverPresenter from "../../../presenter/caregiverPresenter";
import caregiverModel from "../../../model/caregiverModel";

const mapModelStateToComponentState = (patientModelState) => ({
    editedPatient: patientModelState.editedPatient
});

export default class SmartPatientEdit extends Component {
    constructor(props) {
        super(props);
        caregiverPresenter.onInit();
        let editedPatientId = parseInt(props.match.params.id);
        patientPresenter.onInit().then(() => {
            patientModel.setEditedPatient(editedPatientId);
        });
        this.state = {
            editedPatient: []
        };

        this.state = mapModelStateToComponentState(patientModel.state);
        this.listener = () =>
            this.setState(mapModelStateToComponentState(patientModel.state));
        patientModel.addListener("changePatient", this.listener);
    }

    componentWillUnmount() {
        patientModel.removeListener("changePatient", this.listener);
    }

    render() {
        return (
            <PatientAddEdit onChange={patientPresenter.onEditChange}
                            onSubmit={patientPresenter.onEdit}
                            patient={this.state.editedPatient}
                            caregivers={caregiverModel.state.caregivers}

                            mainText="Edit a patient"
                            userModelState={userModel.state}/>
        );
    }
}