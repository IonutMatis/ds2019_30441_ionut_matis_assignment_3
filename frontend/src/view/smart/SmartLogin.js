import React, {Component} from 'react';
import Login from "../dumb/Login";
import userModel from "../../model/userModel";
import userPresenter from "../../presenter/userPresenter";

const mapModelStateToComponentState = (userModel) => ({
    currentUser: userModel.currentUser,
    invalidNameOrPassword: userModel.invalidNameOrPassword
});

export default class SmartLogin extends Component {
    constructor(props) {
        super(props);

        this.state = mapModelStateToComponentState(userModel.state);

        this.listener = (modelState) =>
            this.setState(mapModelStateToComponentState(modelState));
        userModel.addListener("changeUser", this.listener);
    }

    componentWillUnmount() {
        userModel.removeListener("changeUser", this.listener);
    }

    render() {
        return <Login invalidUsernameOrPassword={userModel.state.invalidUsernameOrPassword}
                      currentUser={this.state.currentUser}
                      onChange={userPresenter.onChange}
                      onFormSubmit={userPresenter.onFormSubmit}/>;
    }
}