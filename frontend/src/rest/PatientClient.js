export default class PatientClient {
    constructor(authorization, BASE_URL) {
        this.authorization = authorization;
        this.BASE_URL = BASE_URL;
    }

    loadPatients = () => {
        return fetch(this.BASE_URL + "/patients", {
            method: "GET",
            headers: {
                "Authorization": this.authorization
            }
        }).then(response => response.json())
            .catch((err) => {
                alert("Load patients error");
            });
    };

    addPatient = (name, password, birthDate, gender, address, caregiver) => {
        return fetch(this.BASE_URL + "/patients", {
            method: "POST",
            headers: {
                "Authorization": this.authorization,
                "Content-type": "application/json"
            },
            body: JSON.stringify({
                name: name,
                password: password,
                birthDate: birthDate,
                gender: gender,
                address: address,
                caregiver: caregiver
            })
        }).then(response => {
            if (response.status === 200) {
                return response.json();
            } else {
                alert("Error at creating a patient");
            }
        })
    };

    editPatient(editedPatient) {
        return fetch(this.BASE_URL + "/patients/edit/" + editedPatient.id, {
            method: "PUT",
            headers: {
                "Authorization": this.authorization,
                "Content-type": "application/json"
            },
            body: JSON.stringify({
                name: editedPatient.name,
                password: editedPatient.password,
                birthDate: editedPatient.birthDate,
                gender: editedPatient.gender,
                address: editedPatient.address,
                caregiver: editedPatient.caregiver
            })
        }).then(response => {
            if (response.status === 200) {
                return response.json();
            } else {
                alert("Error at editing a patient");
            }
        })
    }

    getPatientById(patientId) {
        return fetch(this.BASE_URL + "/patients/" + patientId, {
            method: "GET",
            headers: {
                "Authorization": this.authorization
            }
        }).then(response => response.json())
            .catch((err) => {
                alert("Load patients error");
            });
    }

    getMedicationPlans(patientId) {
        return fetch(this.BASE_URL + "/patients/" + patientId + "/medication-plans", {
            method: "GET",
            headers: {
                "Authorization": this.authorization
            }
        }).then(response => response.json())
            .catch((err) => {
                alert("Load patients error");
            });
    }

    deletePatient(patientId) {
        return fetch(this.BASE_URL + "/patients/" + patientId, {
            method: "DELETE",
            headers: {
                "Authorization": this.authorization,
            }
        }).then(response => {
            if (response.status === 200) {
                return response;
            } else {
                alert("Error at deleting patient");
            }
        })
    }
}