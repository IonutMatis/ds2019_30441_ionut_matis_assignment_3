import LoginClient from "./LoginClient";
import PatientClient from "./PatientClient";
import CaregiverClient from "./CaregiverClient";
import MedicationClient from "./MedicationClient";
import MedicationPlanClient from "./MedicationPlanClient";

const BASE_URL = "http://localhost:8080";

export default class ClientFactory {
    constructor(username, password) {
        this.authorization = "Basic " + btoa(username + ":" + password);

        this.loginClient = new LoginClient(this.authorization, BASE_URL);
        this.patientClient = new PatientClient(this.authorization, BASE_URL);
        this.caregiverClient = new CaregiverClient(this.authorization, BASE_URL);
        this.medicationClient = new MedicationClient(this.authorization, BASE_URL);
        this.medicationPlanClient = new MedicationPlanClient(this.authorization, BASE_URL);

    }

    getLoginClient() {
        return this.loginClient;
    }

    getPatientClient() {
        return this.patientClient;
    }

    getCaregiverClient() {
        return this.caregiverClient;
    }

    getMedicationClient() {
        return this.medicationClient;
    }

    getMedicationPlanClient() {
        return this.medicationPlanClient;
    }
}