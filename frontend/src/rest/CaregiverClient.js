export default class CaregiverClient {
    constructor(authorization, BASE_URL) {
        this.authorization = authorization;
        this.BASE_URL = BASE_URL;
    }

    loadCaregivers = () => {
        return fetch(this.BASE_URL + "/caregivers", {
            method: "GET",
            headers: {
                "Authorization": this.authorization
            }
        }).then(response => response.json())
            .catch((err) => {
                alert("Load caregivers error");
            });
    };

    loadPatients = (caregiverId) => {
        return fetch(this.BASE_URL + "/caregivers/" + caregiverId + "/patients", {
            method: "GET",
            headers: {
                "Authorization": this.authorization
            }
        }).then(response => response.json())
            .catch((err) => {
                alert("Load caregivers error");
            });
    };

    addCaregiver = (name, password, birthDate, gender, address) => {
        return fetch(this.BASE_URL + "/caregivers", {
            method: "POST",
            headers: {
                "Authorization": this.authorization,
                "Content-type": "application/json"
            },
            body: JSON.stringify({
                name: name,
                password: password,
                birthDate: birthDate,
                gender: gender,
                address: address
            })
        }).then(response => {
            if (response.status === 200) {
                return response.json();
            } else {
                alert("Error at creating a caregiver");
            }
        })
    };

    editCaregiver(editedCaregiver) {
        return fetch(this.BASE_URL + "/caregivers/edit/" + editedCaregiver.id, {
            method: "PUT",
            headers: {
                "Authorization": this.authorization,
                "Content-type": "application/json"
            },
            body: JSON.stringify({
                name: editedCaregiver.name,
                password: editedCaregiver.password,
                birthDate: editedCaregiver.birthDate,
                gender: editedCaregiver.gender,
                address: editedCaregiver.address
            })
        }).then(response => {
            if (response.status === 200) {
                return response.json();
            } else {
                alert("Error at editing a caregiver");
            }
        })
    }

    deleteCaregiver(caregiverId) {
        return fetch(this.BASE_URL + "/caregivers/" + caregiverId, {
            method: "DELETE",
            headers: {
                "Authorization": this.authorization,
            }
        }).then(response => {
            if (response.status === 200) {
                return response;
            } else {
                alert("Error at deleting caregiver");
            }
        })
    }
}