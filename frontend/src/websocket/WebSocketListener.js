import {EventEmitter} from 'events';
import {Client} from "@stomp/stompjs";

export default class WebSocketListener extends EventEmitter {
    constructor(userId) {
        super();
        this.client = new Client({
            brokerURL: "ws://localhost:8080/api/websocket",
            onConnect: () => {
                this.client.subscribe("/topic/caregiver_" + userId, message => {
                    this.emit("patientActivity", JSON.parse(message.body));
                });
            }
        });
    }
}