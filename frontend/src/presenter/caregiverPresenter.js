import caregiverModel from "../model/caregiverModel";

class CaregiverPresenter {
    onCreate() {
        caregiverModel.addCaregiver()
            .then(() => {
                caregiverModel.resetNewCaregiverProperties();
                window.location.assign("#/caregivers");
            })
    }

    onEdit() {
        caregiverModel.editCaregiver()
            .then(() => {
                window.location.assign("#/caregivers");
            })
    }

    onDelete(caregiverId) {
        caregiverModel.deleteCaregiver(caregiverId);
    }

    onChange(property, value) {
        caregiverModel.changeNewCaregiverProperty(property, value);
    };

    onEditChange(property, value) {
        caregiverModel.changeEditedCaregiverProperty(property, value);
    };

    onInit() {
        caregiverModel.loadCaregivers();
    };

    onLoadPatients() {
        caregiverModel.loadPatientsOfCaregiver();
    }
}

const caregiverPresenter = new CaregiverPresenter();

export default caregiverPresenter;
