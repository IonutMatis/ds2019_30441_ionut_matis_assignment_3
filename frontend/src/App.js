import React from 'react';
import './style/App.css';
import SmartLogin from "./view/smart/SmartLogin";
import ResourceNotFound from "./view/dumb/ResourceNotFound";
import SmartDoctorHomePage from "./view/smart/SmartDoctorHomePage";
import SmartPatientHomePage from "./view/smart/SmartPatientHomePage";
import SmartCaregiverHomePage from "./view/smart/SmartCaregiverHomePage";

import SmartPatientList from "./view/smart/patient/SmartPatientList";
import SmartPatientAdd from "./view/smart/patient/SmartPatientAdd";
import SmartPatientEdit from "./view/smart/patient/SmartPatientEdit";

import SmartCaregiverList from "./view/smart/caregiver/SmartCaregiverList";
import SmartCaregiverAdd from "./view/smart/caregiver/SmartCaregiverAdd";
import SmartCaregiverEdit from "./view/smart/caregiver/SmartCaregiverEdit";

import SmartMedicationList from "./view/smart/medication/SmartMedicationList";
import SmartMedicationAdd from "./view/smart/medication/SmartMedicationAdd";
import SmartMedicationEdit from "./view/smart/medication/SmartMedicationEdit";

import SmartMedicationPlanList from "./view/smart/medicationPlan/SmartMedicationPlanList";
import SmartMedicationPlanAdd from "./view/smart/medicationPlan/SmartMedicationPlanAdd";
import SmartMedicationPlanEdit from "./view/smart/medicationPlan/SmartMedicationPlanEdit";

import {HashRouter, Route, Switch} from "react-router-dom";
import SmartCaregiverPatientList from "./view/smart/caregiver/SmartCaregiverPatientList";
import SmartPatientMedicationPlanList from "./view/smart/patient/SmartPatientMedicationPlanList";
import SmartPatientAccountView from "./view/smart/patient/SmartPatientAccountView";
import ReactNotification from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css'


function App() {
    return (
        <div className="App">
            <ReactNotification />
            <HashRouter>
                <Switch>
                    <Route exact component={SmartLogin} path="/"/>
                    <Route exact component={SmartDoctorHomePage} path="/doctor"/>
                    <Route exact component={SmartCaregiverHomePage} path="/caregiver"/>
                    <Route exact component={SmartPatientHomePage} path="/patient"/>

                    <Route exact component={SmartPatientList} path="/patients"/>
                    <Route exact component={SmartPatientAdd} path="/patients/add"/>
                    <Route exact component={SmartPatientEdit} path="/patients/edit/:id"/>

                    <Route exact component={SmartCaregiverList} path="/caregivers"/>
                    <Route exact component={SmartCaregiverAdd} path="/caregivers/add"/>
                    <Route exact component={SmartCaregiverEdit} path="/caregivers/edit/:id"/>

                    <Route exact component={SmartMedicationList} path="/medications"/>
                    <Route exact component={SmartMedicationAdd} path="/medications/add"/>
                    <Route exact component={SmartMedicationEdit} path="/medications/edit/:id"/>

                    <Route exact component={SmartMedicationPlanList} path="/medication-plans"/>
                    <Route exact component={SmartMedicationPlanAdd} path="/medication-plans/add"/>
                    <Route exact component={SmartMedicationPlanEdit} path="/medication-plans/edit/:id"/>

                    <Route exact component={SmartCaregiverPatientList} path="/my-patients"/>

                    <Route exact component={SmartPatientMedicationPlanList} path="/my-medication-plans"/>
                    <Route exact component={SmartPatientAccountView} path="/my-account"/>


                    <Route component={ResourceNotFound} path="*"/>
                </Switch>
            </HashRouter>
        </div>
    );
}

export default App;
