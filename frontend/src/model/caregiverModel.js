import {EventEmitter} from "events";
import userModel, {getClientFactory} from "./userModel";

class CaregiverModel extends EventEmitter {
    constructor() {
        super();
        this.state = {
            caregivers: [],
            newCaregiver: {
                name: "",
                password: "",
                birthDate: "",
                gender: "MALE",
                address: "",
            },
            editedCaregiver: {
                id: -1,
                name: "",
                password: "",
                birthDate: "",
                gender: "MALE",
                address: "",
            },
            patients: []
        }
    }

    addCaregiver() {
        let newCaregiver = this.state.newCaregiver;
        return getClientFactory().getCaregiverClient().addCaregiver(
            newCaregiver.name,
            newCaregiver.password,
            newCaregiver.birthDate,
            newCaregiver.gender,
            newCaregiver.address
        );
    }

    editCaregiver() {
        let editedCaregiver = this.state.editedCaregiver;
        return getClientFactory().getCaregiverClient().editCaregiver(editedCaregiver)
            .then((editedCaregiver) => {
                this.emit("changeCaregiver", this.state);
                return editedCaregiver;
            })
    }

    loadCaregivers() {
        return getClientFactory().getCaregiverClient().loadCaregivers()
            .then(caregivers => {
                this.state = {
                    ...this.state,
                    caregivers: caregivers
                };
                this.emit("changeCaregiver", this.state);
            });
    }

    loadPatientsOfCaregiver() {
        return getClientFactory().getCaregiverClient().loadPatients(userModel.state.currentUser.id)
            .then((patients) => {
                this.state = {
                    ...this.state,
                    patients: patients
                };
                this.emit("changeCaregiver", this.state);
            })
    }

    deleteCaregiver(caregiverId) {
        getClientFactory().getCaregiverClient().deleteCaregiver(caregiverId)
            .then(response => {
                let remainingCaregivers = this.state.caregivers.filter(
                    caregiver => caregiver.id !== caregiverId
                );
                this.state = {
                    ...this.state,
                    caregivers: remainingCaregivers
                };
                this.emit("changeCaregiver", this.state);
            })
    }

    changeNewCaregiverProperty(property, value) {
        this.state = {
            ...this.state,
            newCaregiver: {
                ...this.state.newCaregiver,
                [property]: value
            }
        };
        this.emit("changeCaregiver", this.state);
    }

    resetNewCaregiverProperties() {
        this.state = {
            ...this.state,
            newCaregiver: {
                name: "",
                password: "",
                birthDate: "",
                gender: "MALE",
                address: "",
            }
        };
        this.emit("changeCaregiver", this.state);
    }

    changeEditedCaregiverProperty(property, value) {
        this.state = {
            ...this.state,
            editedCaregiver: {
                ...this.state.editedCaregiver,
                [property]: value
            }
        };
        this.emit("changeCaregiver", this.state);
    }

    setEditedCaregiver(id) {
        let foundCaregiver = this.state.caregivers
            .filter(caregiver => caregiver.id === id);
        foundCaregiver[0].password = "";
        this.state = {
            ...this.state,
            editedCaregiver: foundCaregiver[0]
        };
        this.emit("changeCaregiver", this.state);
    }
}

const caregiverModel = new CaregiverModel();

export default caregiverModel;