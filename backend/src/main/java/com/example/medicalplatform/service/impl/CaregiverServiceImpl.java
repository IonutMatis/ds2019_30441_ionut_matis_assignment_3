package com.example.medicalplatform.service.impl;

import com.example.medicalplatform.dto.SimpleUserDTO;
import com.example.medicalplatform.dto.caregiver.CaregiverCreateUpdateDTO;
import com.example.medicalplatform.dto.caregiver.CaregiverViewDTO;
import com.example.medicalplatform.dto.patient.PatientSimpleViewDTO;
import com.example.medicalplatform.exception.UnknownCaregiverException;
import com.example.medicalplatform.exception.UnknownPatientException;
import com.example.medicalplatform.model.Caregiver;
import com.example.medicalplatform.model.Patient;
import com.example.medicalplatform.repository.CaregiverRepository;
import com.example.medicalplatform.repository.PatientRepository;
import com.example.medicalplatform.service.CaregiverService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class CaregiverServiceImpl implements CaregiverService {
    private final CaregiverRepository caregiverRepository;
    private final PatientRepository patientRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public CaregiverViewDTO saveUpdate(CaregiverCreateUpdateDTO caregiverCreateUpdateDTO, Long id) {
        Caregiver caregiver = CaregiverCreateUpdateDTO.generateModelFromDTO(caregiverCreateUpdateDTO);
        if (!"".equals(caregiverCreateUpdateDTO.getPassword())) {
            String unencodedPass = caregiverCreateUpdateDTO.getPassword();
            caregiver.setPassword(passwordEncoder.encode(unencodedPass));
        }
        if (id != null) {
            caregiver.setId(id);
        }
        Caregiver savedCaregiver = caregiverRepository.save(caregiver);

        return CaregiverViewDTO.generateDTOFromModel(savedCaregiver);
    }

    @Override
    public CaregiverViewDTO findById(Long id) {
        Caregiver foundCaregiver = caregiverRepository.findById(id).orElseThrow(UnknownCaregiverException::new);

        return CaregiverViewDTO.generateDTOFromModel(foundCaregiver);
    }

    @Override
    public List<CaregiverViewDTO> findAll() {
        return caregiverRepository.findAll().stream()
                .map(CaregiverViewDTO::generateDTOFromModel)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {
        patientRepository.findByCaregiverId(id)
                .forEach(patient -> patient.setCaregiver(null));
        caregiverRepository.deleteById(id);
    }

    @Override
    public List<PatientSimpleViewDTO> findPatientsByCaregiverId(Long caregiverId) {
        return patientRepository.findByCaregiverId(caregiverId).stream()
                .map(PatientSimpleViewDTO::generateDTOFromModel)
                .collect(Collectors.toList());
    }

    @Override
    public CaregiverViewDTO addPatientToCaregiver(SimpleUserDTO<Patient> patient, Long caregiverId) {
        Caregiver caregiver = caregiverRepository.findById(caregiverId).orElseThrow(UnknownCaregiverException::new);
        Patient foundPatient = patientRepository.findById(patient.getId()).orElseThrow(UnknownPatientException::new);

        foundPatient.setCaregiver(caregiver);
        patientRepository.save(foundPatient);
        caregiver.getPatients().add(foundPatient);
        Caregiver savedCaregiver = caregiverRepository.save(caregiver);
        return CaregiverViewDTO.generateDTOFromModel(savedCaregiver);
    }
}
