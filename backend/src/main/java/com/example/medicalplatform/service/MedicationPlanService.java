package com.example.medicalplatform.service;

import com.example.medicalplatform.dto.medicationPlan.MedicationPlanDTO;

import java.util.List;

public interface MedicationPlanService {
    MedicationPlanDTO saveUpdate(MedicationPlanDTO medicationPlanDTO, Long id);

    MedicationPlanDTO findById(Long id);

    List<MedicationPlanDTO> findAll();

    void deleteById(Long id);
}
