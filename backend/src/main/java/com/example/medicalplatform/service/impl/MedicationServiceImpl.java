package com.example.medicalplatform.service.impl;

import com.example.medicalplatform.dto.medication.MedicationDTO;
import com.example.medicalplatform.exception.UnknownMedicationException;
import com.example.medicalplatform.model.Medication;
import com.example.medicalplatform.repository.MedicationRepository;
import com.example.medicalplatform.repository.MedicationToMedicationPlanRepository;
import com.example.medicalplatform.service.MedicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class MedicationServiceImpl implements MedicationService {
    private final MedicationRepository medicationRepository;
    private final MedicationToMedicationPlanRepository medicationToMedicationPlanRepository;

    @Override
    public MedicationDTO saveUpdate(MedicationDTO medicationDTO, Long id) {
        Medication medication = MedicationDTO.generateModelFromDTO(medicationDTO);
        List<String> dtoSideEffects = medicationDTO.getSideEffects();
        medication.setSideEffects(new LinkedHashSet<>(dtoSideEffects));

        if (id != null) {
            medication.setId(id);
        }
        Medication savedMedication = medicationRepository.save(medication);

        return MedicationDTO.generateDTOFromModel(savedMedication);
    }

    @Override
    public MedicationDTO findById(Long id) {
        Medication foundMedication = medicationRepository.findById(id).orElseThrow(UnknownMedicationException::new);

        return MedicationDTO.generateDTOFromModel(foundMedication);
    }

    @Override
    public List<MedicationDTO> findAll() {
        return medicationRepository.findAll().stream()
                .map(MedicationDTO::generateDTOFromModel)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {
        Medication currentMedication = medicationRepository.findById(id)
                .orElseThrow(UnknownMedicationException::new);
        medicationToMedicationPlanRepository.findAll().stream()
                .filter(medicationToMedicationPlan -> medicationToMedicationPlan.getMedication().getName()
                        .equals(currentMedication.getName()))
                .forEach(medicationToMedicationPlan -> medicationToMedicationPlan.setMedication(null));
        medicationRepository.deleteById(id);
    }
}
