package com.example.medicalplatform.service;

import com.example.medicalplatform.dto.medication.MedicationDTO;

import java.util.List;

public interface MedicationService {
    MedicationDTO saveUpdate(MedicationDTO medicationDTO, Long id);

    MedicationDTO findById(Long id);

    List<MedicationDTO> findAll();

    void deleteById(Long id);
}
