package com.example.medicalplatform.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Getter
@Setter
@NoArgsConstructor
@Entity
@DiscriminatorValue("DOCTOR")
public class Doctor extends User {
    public Doctor(Long id) {
        super.setId(id);
    }

    public Doctor(String name, String password) {
        super(null, name, password, UserRole.DOCTOR);
    }
}
