package com.example.medicalplatform.dto.medicationPlan;

import com.example.medicalplatform.dto.SimpleUserDTO;
import com.example.medicalplatform.dto.medication.SimpleMedicationDTO;
import com.example.medicalplatform.model.Doctor;
import com.example.medicalplatform.model.MedicationPlan;
import com.example.medicalplatform.model.MedicationToMedicationPlan;
import com.example.medicalplatform.model.Patient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MedicationPlanDTO {
    private Long id;
    private String description;
    private LocalDate creationDate;
    private SimpleUserDTO<Patient> patient;
    private SimpleUserDTO<Doctor> doctor;
    private List<SimpleMedicationDTO> medications;
    private String intakeIntervals;
    private LocalDate startDate;
    private LocalDate endDate;

    public static MedicationPlan generateModelFromDTO(MedicationPlanDTO dto) {
        MedicationPlan medicationPlan = new MedicationPlan();
        medicationPlan.setId(dto.getId());
        medicationPlan.setDescription(dto.getDescription());
        medicationPlan.setCreationDate(LocalDate.now());
        medicationPlan.setMedicationToMedicationPlans(new ArrayList<>());
        medicationPlan.setStartDate(dto.getStartDate());
        medicationPlan.setEndDate(dto.getEndDate());

        return medicationPlan;
    }

    public static MedicationPlanDTO generateDTOFromModel(MedicationPlan model) {
        MedicationPlanDTO dto = new MedicationPlanDTO();
        dto.setId(model.getId());
        dto.setDescription(model.getDescription());
        dto.setCreationDate(model.getCreationDate());
        dto.setPatient(SimpleUserDTO.generateDTOFromModel(model.getPatient()));
        dto.setDoctor(SimpleUserDTO.generateDTOFromModel(model.getDoctor()));
        dto.setMedications(model.getMedicationToMedicationPlans().stream()
                .map(medicationToMedicationPlan -> SimpleMedicationDTO.generateDTOFromModel(medicationToMedicationPlan.getMedication()))
                .collect(Collectors.toList()));
        String intakes = "";
        for (MedicationToMedicationPlan medicationToMedicationPlan : model.getMedicationToMedicationPlans()) {
            intakes += medicationToMedicationPlan.getIntakeInterval() + " ";
        }
        dto.setIntakeIntervals(intakes.substring(0, intakes.length() - 1));
        dto.setStartDate(model.getStartDate());
        dto.setEndDate(model.getEndDate());

        return dto;
    }
}